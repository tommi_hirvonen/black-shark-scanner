﻿using System;
using System.Text;

namespace BlackSharkScanner
{
    public class UV26 : Ka50Instrument
    {
        protected override string SearchString
        {
            get { return "font_UV_26"; }
        }

        private IntPtr UvPtr { get; set; }

        private string uv;
        public string Uv
        {
            get { return uv; }
            private set
            {
                if (uv == value)
                {
                    return;
                }

                uv = value;
                Changed = true;
            }
        }


        public override void Scan(IntPtr handle, IntPtr start, IntPtr regionSize)
        {
            if (Tracking)
            {
                return;
            }

            var textBytes = Encoding.UTF8.GetBytes(SearchString);
            var buff = new byte[regionSize.ToInt64()];

            var ptr = new IntPtr();

            WindowsApi.ReadProcessMemory(handle, start, buff, regionSize, ref ptr);

            var result = MemData.Scan(buff, textBytes, 0, 0);
            if (result == -1)
            {
                Tracking = false;
                return;
            }

            HandlePtr = handle;
            UvPtr = new IntPtr(start.ToInt64() + result);

            Tracking = true;
        }

        public override bool Read()
        {
            if (HandlePtr == IntPtr.Zero || UvPtr == IntPtr.Zero)
            {
                Tracking = false;
                return false;
            }

            Changed = false;

            var buf = new byte[1];
            var buf2 = new byte[3];

            var ptr = new IntPtr();
            if (!WindowsApi.ReadProcessMemory(HandlePtr, UvPtr + 12 * 16 + 1, buf, new IntPtr(1), ref ptr) ||
                !WindowsApi.ReadProcessMemory(HandlePtr, UvPtr + 41 * 16, buf2, new IntPtr(3), ref ptr))
            {
                Tracking = false;
                return false;
            }
            Uv = buf[0] == 1 ? Encoding.UTF8.GetString(buf2) : "";

            return true;
        }

        public override string DataString(string template)
        {
            return template
                .Replace("uv26", Uv);
        }
    }
}