﻿using System;
using System.Text;

namespace BlackSharkScanner
{
    public class PVI800 : Ka50Instrument
    {
        private string pvi1;
        private string pvi2;
        private string pvi3;
        private string pvi4;

        public string Pvi1
        {
            get { return Pvi1Visible ? pvi1 : ""; }
            private set
            {
                if (pvi1 == value)
                {
                    return;
                }

                pvi1 = value;
                Changed = true;
            }
        }

        public string Pvi2
        {
            get { return Pvi2Visible ? pvi2 : ""; }
            private set
            {
                if (pvi2 == value)
                {
                    return;
                }

                pvi2 = value;
                Changed = true;
            }
        }

        public string Pvi3
        {
            get { return Pvi3Visible ? pvi3 : ""; }
            private set
            {
                if (pvi3 == value)
                {
                    return;
                }

                pvi3 = value;
                Changed = true;
            }
        }

        public string Pvi4
        {
            get { return Pvi4Visible ? pvi4 : ""; }
            private set
            {
                if (pvi4 == value)
                {
                    return;
                }

                pvi4 = value;
                Changed = true;
            }
        }

        private bool Pvi1Visible { get; set; }
        private bool Pvi2Visible { get; set; }
        private bool Pvi3Visible { get; set; }
        private bool Pvi4Visible { get; set; }

        private IntPtr Pvi1Ptr { get; set; }
        private IntPtr Pvi1VisiblePtr { get; set; }
        private IntPtr Pvi2Ptr { get; set; }
        private IntPtr Pvi2VisiblePtr { get; set; }
        private IntPtr Pvi3Ptr { get; set; }
        private IntPtr Pvi3VisiblePtr { get; set; }
        private IntPtr Pvi4Ptr { get; set; }
        private IntPtr Pvi4VisiblePtr { get; set; }

        protected override string SearchString
        {
            get { return "font_PVI"; }
        }

        public override void Scan(IntPtr handle, IntPtr start, IntPtr regionSize)
        {
            if(Tracking)
            {
                return;
            }

            var textBytes = Encoding.UTF8.GetBytes(SearchString);
            var buff = new byte[regionSize.ToInt64()];

            var ptr = new IntPtr();

            WindowsApi.ReadProcessMemory(handle, start, buff, regionSize, ref ptr);

            var result = MemData.Scan(buff, textBytes, 0, 2);
            if(result == -1)
            {
                Tracking = false;
                return;
            }

            HandlePtr = handle;
            Pvi1Ptr = new IntPtr(start.ToInt64() + result + 41 * 16);
            Pvi1VisiblePtr = new IntPtr(start.ToInt64() + result + 12 * 16 + 1);
            result = MemData.Scan(buff, textBytes, result + 1, 0);
            Pvi2Ptr = new IntPtr(start.ToInt64() + result + 41 * 16);
            Pvi2VisiblePtr = new IntPtr(start.ToInt64() + result + 12 * 16 + 1);

            result = MemData.Scan(buff, textBytes, result + 1, 0);
            Pvi3Ptr = new IntPtr(start.ToInt64() + result + 41 * 16);
            Pvi3VisiblePtr = new IntPtr(start.ToInt64() + result + 12 * 16 + 1);

            result = MemData.Scan(buff, textBytes, result + 1, 0);
            Pvi4Ptr = new IntPtr(start.ToInt64() + result + 41 * 16);
            Pvi4VisiblePtr = new IntPtr(start.ToInt64() + result + 12 * 16 + 1);

            Tracking = true;
        }

        public override bool Read()
        {
            if (HandlePtr == IntPtr.Zero || Pvi1Ptr == IntPtr.Zero)
            {
                Tracking = false;
                return false;
            }

            Changed = false;

            var buf = new byte[6];
            var buf2 = new byte[1];
            var buf3 = new byte[1];
            var buf4 = new byte[1];

            var ptr = new IntPtr();
            if (!WindowsApi.ReadProcessMemory(HandlePtr, Pvi1Ptr, buf, new IntPtr(6), ref ptr) ||
                !WindowsApi.ReadProcessMemory(HandlePtr, Pvi1VisiblePtr, buf2, new IntPtr(1), ref ptr) ||
                !WindowsApi.ReadProcessMemory(HandlePtr, Pvi3Ptr, buf3, new IntPtr(1), ref ptr) ||
                !WindowsApi.ReadProcessMemory(HandlePtr, Pvi3VisiblePtr, buf4, new IntPtr(1), ref ptr))
            {
                Tracking = false;
                return false;
            }

            Pvi1 = Encoding.UTF8.GetString(buf);
            Pvi1Visible = buf2[0] == 1;
            Pvi3 = Encoding.UTF8.GetString(buf3);
            Pvi3Visible = buf4[0] == 1;

            if (!WindowsApi.ReadProcessMemory(HandlePtr, Pvi2Ptr, buf, new IntPtr(6), ref ptr) ||
                !WindowsApi.ReadProcessMemory(HandlePtr, Pvi2VisiblePtr, buf2, new IntPtr(1), ref ptr) ||
                !WindowsApi.ReadProcessMemory(HandlePtr, Pvi4Ptr, buf3, new IntPtr(1), ref ptr) ||
                !WindowsApi.ReadProcessMemory(HandlePtr, Pvi4VisiblePtr, buf4, new IntPtr(1), ref ptr))
            {
                Tracking = false;
                return false;
            }

            Pvi2 = Encoding.UTF8.GetString(buf);
            Pvi2Visible = buf2[0] == 1;
            Pvi4 = Encoding.UTF8.GetString(buf3);
            Pvi4Visible = buf4[0] == 1;

            return true;
        }

        public override string DataString(string template)
        {
            return template
                .Replace("pvi1", Pvi1)
                .Replace("pvi2", Pvi2)
                .Replace("pvi3", Pvi3)
                .Replace("pvi4", Pvi4);
        } 
    }
}