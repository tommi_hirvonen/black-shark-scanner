using System;

namespace BlackSharkScanner
{
    public class MemInfo
    {
        public IntPtr Handle { get; set; }
        public IntPtr Pvi1 { get; set; }
        public IntPtr Pvi1Visible { get; set; }
        public IntPtr Pvi2 { get; set; }
        public IntPtr Pvi2Visible { get; set; }
        public IntPtr Pvi3 { get; set; }
        public IntPtr Pvi3Visible { get; set; }
        public IntPtr Pvi4 { get; set; }
        public IntPtr Pvi4Visible { get; set; }
    }
}