using System.IO;
using Newtonsoft.Json;

namespace BlackSharkScanner
{
    public class Settings
    {
        public string ProcessName { get; set; }
        public int MemRegStart { get; set; }
        public string Ip { get; set; }
        public int Port { get; set; }
        public string Template { get; set; }

        public static Settings Read()
        {
            return JsonConvert.DeserializeObject<Settings>(File.ReadAllText("settings.json"));
        }
    }
}