namespace BlackSharkScanner
{
    public class MemData
    {
        public static int Scan(byte[] sIn, byte[] sFor, int offset, int skipFinds, bool additionalKA50Check = true)
        {
            var len = sIn.Length;
            for (var i = offset; i < len; i++)
            {
                var found = true;
                for (var j = 0; j < sFor.Length; j++)
                {
                    if (i + j >= len || (sFor[j] != 999 && sFor[j] != 555 && sIn[i + j] != sFor[j]) || ((sFor[j] == 999 || sFor[j] == 555) && sIn[i + j] == 0))
                    {
                        found = false;
                        break;
                    }
                    if (additionalKA50Check && (i < 15 || sIn[i - 15] != 0 || sIn[i - 14] != 0 || sIn[i - 13] != 0 || sIn[i - 12] != 0 || sIn[i - 11] != 0))
                    {
                        found = false;
                        break;
                    }
                }
                if (found && skipFinds-- == 0)
                {
                    return i;
                }
            }
            return -1;
        }
    }
}