using System;

namespace BlackSharkScanner
{
    public abstract class Ka50Instrument {
        public bool Changed { get; protected set; }
        public bool Tracking { get; protected set; }

        public abstract void Scan(IntPtr handle, IntPtr start, IntPtr regionSize);
        public abstract bool Read();
        public abstract string DataString(string template);

        protected abstract string SearchString { get; }
        protected IntPtr HandlePtr { get; set; }
    }
}