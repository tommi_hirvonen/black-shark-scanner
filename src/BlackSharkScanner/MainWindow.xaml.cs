﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Timers;
using System.Windows;

namespace BlackSharkScanner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<WindowsApi.MEMORY_BASIC_INFORMATION> MemReg { get; set; }
        private UdpClient udpClient;
        private IPEndPoint sendEndPoint;
        private PVI800 pvi800;
        private PUI800 pui800;
        private UV26 uv26;

        public void MemInfo(IntPtr pHandle)
        {
            var addy = new IntPtr();
            while (true)
            {
                var memInfo = new WindowsApi.MEMORY_BASIC_INFORMATION();
                var memDump = WindowsApi.VirtualQueryEx(pHandle, addy, ref memInfo, Marshal.SizeOf(memInfo));
                if (memDump == 0) break;
                if ((memInfo.State & 0x1000) != 0 && (memInfo.Protect & 0x100) == 0)
                    MemReg.Add(memInfo);
                addy = new IntPtr(memInfo.BaseAddress.ToInt64() + memInfo.RegionSize.ToInt64());
            }
        }

        private static Timer aTimer;
        private static Timer bTimer;
        private readonly Settings settings;

        public MainWindow()
        {
            InitializeComponent();

            settings = Settings.Read();

            udpClient = new UdpClient();
            sendEndPoint = new IPEndPoint(IPAddress.Parse(settings.Ip), settings.Port);
            IPAddressField.Text = settings.Ip;
            PortField.Text = settings.Port.ToString(CultureInfo.InvariantCulture);

            pvi800 = new PVI800();
            pui800 = new PUI800();
            uv26 = new UV26();
            SetStatus("Searching for DCS process...");

            aTimer = new Timer(2000) {AutoReset = false, Enabled = true};
            bTimer = new Timer(200) {AutoReset = true};
            aTimer.Elapsed += Start;
            bTimer.Elapsed += Read;
        }

        public void Start(object source, ElapsedEventArgs e)
        {
            var processes = Process.GetProcessesByName(settings.ProcessName);

            if(!processes.Any())
            {
                SetStatus("Searching for DCS process...");
                ResetTimer();
                return;
            }

            SetStatus("Scanning memory...");

            MemReg = new List<WindowsApi.MEMORY_BASIC_INFORMATION>();
            MemInfo(processes[0].Handle);

            for (var i = settings.MemRegStart; i < MemReg.Count; i++)
            {
                pvi800.Scan(processes[0].Handle, MemReg[i].BaseAddress, MemReg[i].RegionSize);
                pui800.Scan(processes[0].Handle, MemReg[i].BaseAddress, MemReg[i].RegionSize);
                uv26.Scan(processes[0].Handle, MemReg[i].BaseAddress, MemReg[i].RegionSize);

                if (!pvi800.Tracking || !pui800.Tracking || !uv26.Tracking)
                {
                    continue;
                }

                SetStatus("Tracking data");

                bTimer.Start();
                return;
            }

            SetStatus("Data not found");
            ResetTimer();
        }

        public void Read(object source, ElapsedEventArgs e)
        {
            if (pvi800.Read() && pui800.Read() && uv26.Read())
            {
                SetPvi(pvi800.Pvi1 + " " + pvi800.Pvi3);
                SetPvi2(pvi800.Pvi2 + " " + pvi800.Pvi4);
                SetPui(pui800.Pui1 + " " + pui800.Pui2 + " " + pui800.Pui3);
                SetUv(uv26.Uv);

                if(pvi800.Changed || pui800.Changed || uv26.Changed)
                {
                    SendData();
                }
            }
            else
            {
                SetStatus("Searching for DCS process...");
                bTimer.Stop();
                SetPvi("");
                SetPvi2("");
                ResetTimer();
            }
        }

        private void SetStatus(string status)
        {
            StatusLabel.Dispatcher.BeginInvoke(new Action(delegate
                                                          {
                                                              StatusLabel.Content = status;
                                                          }));
        }

        private void SetPvi(string value)
        {
            PVI.Dispatcher.BeginInvoke(new Action(delegate
                                                  {
                                                      PVI.Content = value;
                                                  }));
        }

        private void SetPvi2(string value)
        {
            PVI2.Dispatcher.BeginInvoke(new Action(delegate
                                                   {
                                                       PVI2.Content = value;
                                                   }));
        }

        private void SetPui(string value)
        {
            PUI.Dispatcher.BeginInvoke(new Action(delegate
                                                  {
                                                      PUI.Content = value;
                                                  }));
        }

        private void SetUv(string value)
        {
            UV26.Dispatcher.BeginInvoke(new Action(delegate
            {
                UV26.Content = value;
            }));
        }

        private void ResetTimer()
        {
            aTimer.Stop();
            aTimer.Start();
        }

        private void SendData()
        {
            var dataString = settings.Template;
            dataString = pvi800.DataString(dataString);
            dataString = pui800.DataString(dataString);
            dataString = uv26.DataString(dataString);

            var bytes = Encoding.UTF8.GetBytes(dataString);
            udpClient.Send(bytes, bytes.Length, sendEndPoint);
        }
    }
}
