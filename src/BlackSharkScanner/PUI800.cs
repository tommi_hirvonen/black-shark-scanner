﻿using System;
using System.Text;
using System.Linq;

namespace BlackSharkScanner
{
    public class PUI800 : Ka50Instrument
    {
        private string pui1;
        private string pui2;
        private string pui3;

        public string Pui1
        {
            get { return pui1; }
            private set
            {
                if (pui1 == value)
                {
                    return;
                }
                pui1 = value;
                Changed = true;
            }
        }

        public string Pui2
        {
            get { return pui2; }
            private set
            {
                if (pui2 == value)
                {
                    return;
                }
                pui2 = value;
                Changed = true;
            }
        }
        public string Pui3
        {
            get { return pui3; }
            private set
            {
                if (pui3 == value)
                {
                    return;
                }
                pui3 = value;
                Changed = true;
            }
        }

        protected override string SearchString
        {
            get { return "font_PUI"; }
        }

        private int puiPage;
        private IntPtr[] PagePtrs { get; set; }

        public PUI800()
        {
            PagePtrs = new IntPtr[6];
        }

        public override void Scan(IntPtr handle, IntPtr start, IntPtr regionSize)
        {
            if (Tracking)
            {
                return;
            }

            var textBytes = Encoding.UTF8.GetBytes(SearchString);
            var buff = new byte[regionSize.ToInt64()];

            var ptr = new IntPtr();

            WindowsApi.ReadProcessMemory(handle, start, buff, regionSize, ref ptr);

            var result = MemData.Scan(buff, textBytes, 0, 0);
            if (result == -1)
            {
                Tracking = false;
                return;
            }

            HandlePtr = handle;
            PagePtrs[0] = new IntPtr(start.ToInt64() + result);
            for (var i = 1; i < 6; i++)
            {
                PagePtrs[i] = new IntPtr(start.ToInt64() + MemData.Scan(buff, textBytes, result, i));
            }

            Tracking = true;
        }

        public override bool Read()
        {
            if (HandlePtr == IntPtr.Zero)
            {
                Tracking = false;
                return false;
            }

            Changed = false;

            var buf = new byte[1];
            var buf2 = new byte[1];

            var buf3 = new byte[4];
            var buf4 = new byte[2];
            var buf5= new byte[2];
            var ptr = new IntPtr();
            puiPage = -1;
            for (var i = 0; i < 4; i++)
            {
                if (!WindowsApi.ReadProcessMemory(HandlePtr, PagePtrs[i] + 12*16 + 1, buf, new IntPtr(1), ref ptr) ||
                    !WindowsApi.ReadProcessMemory(HandlePtr, PagePtrs[i] + 41*16, buf3, new IntPtr(4), ref ptr))
                {
                    Tracking = false;
                    return false;
                }

                if (buf[0] == 1)
                {
                    puiPage = i;
                    Pui1 = WeaponString(buf3);
                }
            }
            if(puiPage == -1)
            {
                Pui1 = "";
            }

            if (!WindowsApi.ReadProcessMemory(HandlePtr, PagePtrs[4] + 12 * 16 + 1, buf, new IntPtr(1), ref ptr) ||
                !WindowsApi.ReadProcessMemory(HandlePtr, PagePtrs[4] + 41 * 16, buf4, new IntPtr(2), ref ptr) ||
                !WindowsApi.ReadProcessMemory(HandlePtr, PagePtrs[5] + 12 * 16 + 1, buf2, new IntPtr(1), ref ptr) ||
                !WindowsApi.ReadProcessMemory(HandlePtr, PagePtrs[5] + 41 * 16, buf5, new IntPtr(2), ref ptr))
            {
                Tracking = false;
                return false;
            }

            Pui2 = buf[0] == 1 ? Encoding.UTF8.GetString(buf4) : "";
            Pui3 = buf2[0] == 1 ? Encoding.UTF8.GetString(buf5) : "";

            return true;
        }

        public override string DataString(string template)
        {
            return template
                .Replace("pui1", Pui1)
                .Replace("pui2", Pui2)
                .Replace("pui3", Pui3);
        }

        private string WeaponString(byte[] bytes)
        {
            if(bytes.SequenceEqual(new byte[] {208, 157, 208, 160}))
            {
                return "HP";
            }
            if(bytes.SequenceEqual(new byte[] {208, 159, 208, 161}))
            {
                return "NC";
            }
            if(bytes[0] == 65 && bytes[1] == 54)
            {
                return "A6";
            }
            return "N6";
        }
    }
}