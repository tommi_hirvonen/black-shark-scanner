# Memory scanner for DCS Black Shark 2

![Screenshot](https://dl.dropboxusercontent.com/u/19756841/scanner.png)

## Features

* Reads PVI-800, PUI-800 and UV26 display values from BS2 and displays them on screen
* Exports values with UDP

## TODO

* Other displays?
* 32bit support?
	
## Usage

* Start the .exe, start Black Shark and enjoy. Initial scan might take a while (~1-30 seconds) and might use quite much CPU, don't get scared.
* If the scanner doesn't find data ("Data not found" displayed on the status text), try lowering the memRegStart setting in settings.json file
	* You can also try increasing the value to speed up the initial scan
* Feel free to (mis)use/modify/distribute etc the code in any way you wish

## Settings

settings.json contains the following settings

* Ip: IP address to send the data
* Port: Port to send the data
* Template: Format of the data string sent with UDP. Keywords (pvi1 etc) are replaced with the values.
* ProcessName: The name of the DCS process the scanner is looking for
* MemRegStart: The memory region where the scan should start. Correct values are usually found around 1170-1180, but might be lower/higher. Adjust as needed.
	* Setting this too low will cause the initial scan to take very long and setting this too high will cause the scan to miss the values and fail.

## Download

[Download the zip](https://dl.dropboxusercontent.com/u/19756841/BlackSharkScanner.zip)

## Thanks
* Oleksiy Gapotchenko for [this blogpost.](http://eazfuscator.blogspot.fi/2011/06/reading-environment-variables-from.html)